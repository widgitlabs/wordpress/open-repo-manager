<?php
/**
 * Core unit test
 *
 * @package     Repo_Manager\Tests\Core
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Core unit tests
 *
 * @since       1.0.0
 */
class Tests_Repo_Manager extends WP_UnitTestCase {


	/**
	 * Test suite object
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @var         object $object The test suite object
	 */
	protected $object;


	/**
	 * Set up this test suite
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function setUp() {
		parent::setUp();
		$this->object = Repo_Manager();
	}


	/**
	 * Tear down this test suite
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function tearDown() { // phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod
		parent::tearDown();
	}


	/**
	 * Test the Repo_Manager instance
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function test_repo_manager_instance() {
		$this->assertClassHasStaticAttribute( 'instance', 'Repo_Manager' );
	}


	/**
	 * Test constants
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function test_constants() {
		// Make SURE I updated the version.
		$this->assertSame( REPO_MANAGER_VER, '1.0.0' );

		// Plugin folder URL.
		$path = str_replace( 'tests/', '', plugin_dir_url( __FILE__ ) );
		$this->assertSame( REPO_MANAGER_URL, $path );

		// Plugin folder path.
		$path         = str_replace( 'tests/', '', plugin_dir_path( __FILE__ ) );
		$path         = substr( $path, 0, -1 );
		$repo_manager = substr( REPO_MANAGER_DIR, 0, -1 );
		$this->assertSame( $repo_manager, $path );

		// Plugin root file.
		$path = str_replace( 'tests/', '', plugin_dir_path( __FILE__ ) );
		$this->assertSame( REPO_MANAGER_FILE, $path . 'class-repo-manager.php' );
	}


	/**
	 * Test includes
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function test_includes() {
		$this->assertFileExists( REPO_MANAGER_DIR . 'class-repo-manager.php' );
		$this->assertFileExists( REPO_MANAGER_DIR . 'includes/admin/settings/register-settings.php' );

		/**
		 * Todo: Remove if not needed.
		 *
		 * // $this->assertFileExists( REPO_MANAGER_DIR . 'includes/admin/actions.php' );
		 *
		 * Check Assets Exist
		 * // $this->assertFileExists( USERNAME_CHANGER_DIR . 'assets/css/admin.css' );
		 * // $this->assertFileExists( USERNAME_CHANGER_DIR . 'assets/css/admin.min.css' );
		 * // $this->assertFileExists( USERNAME_CHANGER_DIR . 'assets/js/admin.js' );
		 * // $this->assertFileExists( USERNAME_CHANGER_DIR . 'assets/js/admin.min.js' );
		 */
	}
}
