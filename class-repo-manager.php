<?php
/**
 * Plugin Name:     Open Repo Manager
 * Plugin URI:      https://openrepomanager.com
 * Description:     Modular repository integration for WordPress
 * Author:          Pop!_Planet Team
 * Author URI:      https://pop-planet.info
 * Version:         1.0.0
 * Text Domain:     repo-manager
 * Domain Path:     languages
 *
 * @package         Repo_Manager
 * @author          Daniel J Griffiths <dgriffiths@evertiro.com>
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( ! class_exists( 'Repo_Manager' ) ) {


	/**
	 * Main Repo_Manager class
	 *
	 * @access      public
	 * @since       1.0.0
	 */
	final class Repo_Manager {


		/**
		 * The one true Repo_Manager
		 *
		 * @access      private
		 * @since       1.0.0
		 * @var         Repo_Manager $instance The one true Repo_Manager
		 */
		private static $instance;


		/**
		 * The settings object
		 *
		 * @access      public
		 * @since       1.0.0
		 * @var         object $settings The settings object
		 */
		public $settings;


		/**
		 * Get active instance
		 *
		 * @access      public
		 * @since       1.0.0
		 * @static
		 * @return      object self::$instance The one true Repo_Manager
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Repo_Manager ) ) {
				self::$instance = new Repo_Manager();
				self::$instance->setup_constants();
				self::$instance->hooks();
				self::$instance->includes();
			}

			return self::$instance;
		}


		/**
		 * Throw error on object clone
		 *
		 * The whole idea of the singleton design pattern is that there is
		 * a single object. Therefore, we don't want the object to be cloned.
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'repo-manager' ), '1.0.0' );
		}


		/**
		 * Disable unserializing of the class
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'repo-manager' ), '1.0.0' );
		}


		/**
		 * Setup plugin constants
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function setup_constants() {
			// Plugin version.
			if ( ! defined( 'REPO_MANAGER_VER' ) ) {
				define( 'REPO_MANAGER_VER', '1.0.0' );
			}

			// Plugin path.
			if ( ! defined( 'REPO_MANAGER_DIR' ) ) {
				define( 'REPO_MANAGER_DIR', plugin_dir_path( __FILE__ ) );
			}

			// Plugin URL.
			if ( ! defined( 'REPO_MANAGER_URL' ) ) {
				define( 'REPO_MANAGER_URL', plugin_dir_url( __FILE__ ) );
			}

			// Plugin file.
			if ( ! defined( 'REPO_MANAGER_FILE' ) ) {
				define( 'REPO_MANAGER_FILE', __FILE__ );
			}
		}


		/**
		 * Run plugin base hooks
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function hooks() {
			add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
		}


		/**
		 * Include necessary files
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function includes() {
			global $repo_manager_options;

			// Load settings handler if necessary.
			if ( ! class_exists( 'Simple_Settings' ) ) {
				require_once REPO_MANAGER_DIR . 'includes/libraries/simple-settings/class-simple-settings.php';
			}

			require_once REPO_MANAGER_DIR . 'includes/admin/settings/register-settings.php';

			self::$instance->settings = new Simple_Settings( 'repo_manager', 'settings' );
			$repo_manager_options     = self::$instance->settings->get_settings();

			/*
			TODO: Remove if not needed.
			if ( is_admin() ) {
				require_once REPO_MANAGER_DIR . 'includes/admin/actions.php';
			}
			*/
		}


		/**
		 * Load plugin language files
		 *
		 * @access      public
		 * @since       1.0.0
		 * @return      void
		 */
		public function load_textdomain() {
			// Set filter for language directory.
			$lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
			$lang_dir = apply_filters( 'repo_manager_languages_directory', $lang_dir );

			// WordPress plugin locale filter.
			$locale = apply_filters( 'plugin_locale', get_locale(), 'repo-manager' );
			$mofile = sprintf( '%1$s-%2$s.mo', 'repo-manager', $locale );

			// Setup paths to current locale file.
			$mofile_local  = $lang_dir . $mofile;
			$mofile_global = WP_LANG_DIR . '/repo-manager/' . $mofile;
			$mofile_core   = WP_LANG_DIR . '/plugins/repo-manager/' . $mofile;

			if ( file_exists( $mofile_global ) ) {
				// Look in global /wp-content/languages/repo-manager folder.
				load_textdomain( 'repo-manager', $mofile_global );
			} elseif ( file_exists( $mofile_local ) ) {
				// Look in local /wp-content/plugins/repo-manager/languages/ folder.
				load_textdomain( 'repo-manager', $mofile_local );
			} elseif ( file_exists( $mofile_core ) ) {
				// Look in core /wp-content/languages/plugins/repo-manager/ folder.
				load_textdomain( 'repo-manager', $mofile_core );
			} else {
				// Load the default language files.
				load_plugin_textdomain( 'repo-manager', false, $lang_dir );
			}
		}
	}
}


/**
 * The main function responsible for returning the one true Repo_Manager
 * instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without
 * needing to declare the global.
 *
 * Example: <?php $repo_manager = Repo_Manager(); ?>
 *
 * @since       1.0.0
 * @return      Repo_Manager The one true Repo_Manager
 */
function repo_manager() {
	return Repo_Manager::instance();
}

// Get things started.
Repo_Manager();
