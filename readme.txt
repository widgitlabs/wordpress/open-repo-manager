=== Open Repo Manager ===
Contributors: evertiro
Donate link: https://evertiro.com/donate/
Tags: apt, repo, repository, aptly, debian, ubuntu
Requires at least: 3.0
Tested up to: 5.3.2
Stable tag: 1.0.0

Modular repository integration for WordPress

== Description ==

Maintaining a functional APT repo is hard enough. Launching a package management site for it is even worse. There are very few platforms out there capable of building a repository browser that actually _looks_ good, let alone works properly. Open Repo Manager is an attempt to change that. Leveraging the flexibility of WordPress, you can launch your own APT repo with ease!

== Installation ==

= From your WordPress dashboard =

1. Visit 'Plugins > Add New'
2. Click 'Upload Plugin'
3. Upload the provided zip file
4. Activate Open Repo Manager from your Plugins page

== Frequently Asked Questions ==

= Does Open Repo Manager support reprepro? =

Not at this time. Currently, ORM only supports repos managed by Aptly. Further systems will be added in the future.

== Screenshots ==

== Changelog ==

= Version 1.0.0 =
* Initial release
